#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "../../config.h"
#include "network.h"
#include "command.h"
#include "widget.h"
#include "font.h"
#include "theme.h"

void usage()
{
	printf("Usage:  xdockserver [options]\n");
	printf("   -y  --vertical-position   Define the number of vertical pixels the first\n"
	       "                             dock will skip. This can be used when there\'s\n"
	       "                             already a taskbar on the screen.\n"
	       "   -h  --help                Display this message.\n"
	       "   -v  --version             Display version and exit.\n");
}

int main(int argc, char** argv)
{
	pthread_t event_thread;

	int next_option;
	const char* const short_options = "y:hv";
	const struct option long_options[] = {
		{ "vertical-skil", required_argument, 0, 'y' },
		{ "version",       no_argument,       0, 'v' },
		{ "help",          no_argument,       0, 'h' },
		{ 0, 0, 0, 0  }
	};

	y_skip = 0;

	// get options
	do
	{
		next_option = getopt_long(argc, argv, short_options, long_options, NULL);
		switch(next_option)
		{
			case 'h': // help
				usage();
				return 0;

			case 'v': // version
				printf("xdockserver " VERSION "\n");
				return 0;

			case 'y':
				y_skip = atoi(optarg);
				break;

			case '?':
				return -1;

			case -1: // done with options
				break;
		}
	} while(next_option != -1);

	// run the software
	if(!create_window())
		return 1;
	load_fonts();
	initialize_themes();
	// pthread_create(&event_thread, NULL, do_events, NULL);
	// network_listen();
	pthread_create(&event_thread, NULL, network_listen, NULL);
	do_events();
	return 0;
}
