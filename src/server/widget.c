#include "widget.h"
#include "../lib/xdock.h"
#include "square.xpm"
#include "font.h"
#include "event.h"
#include "theme.h"

#include <Imlib.h>
#include <stdio.h>

static void create_faux_window();

Display* display;
int n_clients;
ImlibData* imlib_data;
int screen_w, screen_h;
int white;
Window faux_window;
XColor faux_bg;

inline void get_size(int *x1, int *y1, int *x2, int *y2, int *w, int *h)
{
	if(x1 > x2)
	{
		int t = *x1;
		*x1 = *x2;
		*x2 = t;
	}
	*w = *x2 - *x1 + 1;

	if(y1 > y2)
	{
		int t = *y1;
		*y1 = *y2;
		*y2 = t;
	}
	*h = *y2 - *y1 + 1;
}

Pixmap load_xpm(char** xpm)
{
	ImlibImage* image = Imlib_create_image_from_xpm_data(
			imlib_data, xpm);
	Imlib_render(imlib_data, image, image->rgb_width, image->rgb_height);
	return Imlib_copy_image(imlib_data, image);
}

static void add_color(client_t* client, int n, const char* c)
{
	XParseColor(display, client->colormap, c, &client->color[n]);
	XAllocColor(display, client->colormap, &client->color[n]);
}

static void initialize_colormap(client_t* client)
{
	client->colormap = DefaultColormap(display, 0);
	add_color(client, XD_BLACK,   "#000000");
	add_color(client, XD_GRAY_10, "#191919");
	add_color(client, XD_GRAY_20, "#333333");
	add_color(client, XD_GRAY_30, "#4C4C4C");
	add_color(client, XD_GRAY_40, "#656565");
	add_color(client, XD_GRAY_50, "#7F7F7F");
	add_color(client, XD_GRAY_60, "#989898");
	add_color(client, XD_GRAY_70, "#B1B1B1");
	add_color(client, XD_GRAY_80, "#CACACA");
	add_color(client, XD_GRAY_90, "#E3E3E3");
	add_color(client, XD_WHITE,   "#FFFFFF");

	add_color(client, XD_LED_BG,      "#1B1B1B");
	add_color(client, XD_LED_UNLIT_1, "#004840");
	add_color(client, XD_LED_UNLIT_2, "#007C70");
	add_color(client, XD_LED_LIT_1,   "#188880");
	add_color(client, XD_LED_LIT_2,   "#20B0A8");
	add_color(client, XD_LED_GLOW,    "#00E800");
}

int set_color(client_t* client, int c, int r, int g, int b)
{
	char cl[8];
	sprintf(cl, "#%02X%02X%02X", r, g, b);
	add_color(client, c, cl);
	return 1;
}

int create_window()
{
	XInitThreads();

	display = XOpenDisplay(NULL);
	if(!display)
	{
		fprintf(stderr, "Could not open display.\n");
		return 0;
	}

	imlib_data = Imlib_init(display);

	white = WhitePixel(display, DefaultScreen(display));

	screen_w = XDisplayWidth(display, DefaultScreen(display));
	screen_h = XDisplayHeight(display, DefaultScreen(display));

	max_positions = screen_h / 64;

	create_faux_window();

	return 1;
}

int draw_new_client(client_t* client)
{
	Atom atoms[2] = { None, None };
	Atom strut;
	unsigned long struts[12] = { 0, };

	client->window = XCreateSimpleWindow(
			display,
			DefaultRootWindow(display),
			screen_w - 64, 0, // position
			64, 1,
			0,
			white, white);

	XMapWindow(display, client->window);

	atoms[0] = XInternAtom(display, "_NET_WM_WINDOW_TYPE_DOCK", False);
	XChangeProperty(display, client->window,
			XInternAtom(display, "_NET_WM_WINDOW_TYPE", False),
			XA_ATOM, 32, PropModeReplace,
			(unsigned char*) atoms,
			1);

	strut = XInternAtom(display, "_NET_WM_STRUT_PARTIAL", False);
	struts[1] = 64;
	struts[6] = 64;
	struts[7] = 64;
	XChangeProperty(display, client->window, strut, XA_CARDINAL, 32, PropModeReplace,
			(unsigned char*)&struts, 12);

	move_client(client);
	XResizeWindow(display, client->window, 64, 64);
	
	client->gc = XCreateGC(display, client->window, 0, NULL);

	initialize_colormap(client);

	n_clients = 0;

	client->pixmap = load_xpm(square_xpm);
	client->image = malloc(1);

	XSelectInput(display, client->window, 
			  ExposureMask 
			| StructureNotifyMask
			| PointerMotionMask
			| ButtonPressMask
			| ButtonReleaseMask);

	return -1;
}

void undraw_client(client_t* client)
{
	// XUnmapWindow(display, client->window);
	XDestroyWindow(display, client->window);
}

static client_t* get_client_from_window(Window window)
{
	client_t* current = first_client;
	while(current)
	{
		if(current->window == window)
			return current;
		current = current->next;
	}
	fprintf(stderr, "Error: window not found\n");
	return NULL;
}

void do_events()
{
	XEvent event;
	unsigned char mask;

	while(1)
	{
		XNextEvent(display, &event);
		switch(event.type)
		{
			case Expose:
				draw_update(get_client_from_window(event.xexpose.window));
				break;

			case MapNotify:
				break;

			case UnmapNotify:
				break;

			case DestroyNotify:
				break;

			case MotionNotify:
				mask = 0;
				if(event.xmotion.state & (1L<<8))
					mask = 0x1;
				if(event.xmotion.state & (1L<<9))
					mask |= 0x2;
				if(event.xmotion.state & (1L<<10))
					mask |= 0x4;
				mouse_move(get_client_from_window(event.xmotion.window),
						event.xmotion.x, event.xmotion.y, mask,
						event.xmotion.x_root, event.xmotion.y_root);
				break;

			case ButtonPress:
				mouse_down(get_client_from_window(event.xbutton.window),
						event.xbutton.x, event.xbutton.y, event.xbutton.button);
				break;

			case ButtonRelease:
				mouse_up(get_client_from_window(event.xbutton.window),
						event.xbutton.x, event.xbutton.y, event.xbutton.button,
						event.xbutton.x_root, event.xbutton.y_root);
				break;
		}
	}
}

int draw_update(client_t* client)
{
	if(client)
		XCopyArea(display, client->pixmap, client->window, client->gc, 0, 0, 64, 64, 0, 0);
	XFlush(display);
	return -1;
}

int draw_pixel(client_t* client, int cl, int x, int y)
{
	// TODO check if color validated
	XSetForeground(display, client->gc, client->color[cl].pixel);
	XDrawPoint(display, client->pixmap, client->gc, x, y);

	return -1;
}

int draw_line(client_t* client, int cl, int x1, int y1, int x2, int y2)
{
	XSetForeground(display, client->gc, client->color[cl].pixel);
	XDrawLine(display, client->pixmap, client->gc, x1, y1, x2, y2);

	return -1;
}

int draw_rectangle(client_t* client, int cl, int x1, int y1, int x2, int y2)
{
	int w, h;
	get_size(&x1, &y1, &x2, &y2, &w, &h);
	XSetForeground(display, client->gc, client->color[cl].pixel);
	XDrawRectangle(display, client->pixmap, client->gc, x1, y1, w, h);

	return -1;
}

int draw_box(client_t* client, int fcl, int bcl, int x1, int y1, int x2, int y2)
{
	int w, h;
	get_size(&x1, &y1, &x2, &y2, &w, &h);
	XSetForeground(display, client->gc, client->color[fcl].pixel);
	XSetBackground(display, client->gc, client->color[bcl].pixel);
	XFillRectangle(display, client->pixmap, client->gc, x1, y1, w, h);

	return -1;
}

int write_text(client_t* client, int theme, int font, int x, int y, unsigned char *text)
{
	int i = 0;
	unsigned int w, h;
	Window tmpw;
	int t;
	unsigned int ut;

	Pixmap* f = prepare_font(theme, font);

	while(i < strlen((char*)text))
	{
		if(f[text[i]])
		{
			XGetGeometry(display, f[text[i]], &tmpw, &t, &t,
					&w, &h, &ut, &ut);
			if(x > 0 && (x+w) < 64 && y > 0 && (y+h) < 64)
				XCopyArea(display, f[text[i]], client->pixmap, client->gc,
						0, 0, w, h, x, y);
			x += w;
		}
		i++;
	}

	return -1;
}

int move_box(client_t* client, int x, int y, int x2, int y2, int direction, int s, int bg_color)
{
	int w, h;
	get_size(&x, &y, &x2, &y2, &w, &h);

	XSetForeground(display, client->gc, client->color[bg_color].pixel);

	switch(direction)
	{
		case XD_LEFT:
			XCopyArea(display, client->pixmap, client->pixmap, client->gc, 
					x+s, y, w-1, h, x, y);
			XDrawLine(display, client->pixmap, client->gc, x+w-s, y, x+w-s, y+h-s);
			break;
		case XD_RIGHT:
			XCopyArea(display, client->pixmap, client->pixmap, client->gc, 
					x, y, w-s, h, x+s, y);
			XDrawLine(display, client->pixmap, client->gc, x, y, x, y+h-s);
			break;
		case XD_DOWN:
			XCopyArea(display, client->pixmap, client->pixmap, client->gc, 
					x, y, w, h-s, x, y+s);
			XDrawLine(display, client->pixmap, client->gc, x, y, x+w-s, y);
			break;
		case XD_UP:
			XCopyArea(display, client->pixmap, client->pixmap, client->gc, 
					x, y+s, w, h-s, x, y);
			XDrawLine(display, client->pixmap, client->gc, x, y+h-s, x+w-s, y+h-s);
			break;
		default:
			return 0;
	}

	return 1;
}

int new_pixmap(client_t* client, int number, int bytes, unsigned char* xpm)
{
	char** xpm_pixmap;
	int c, j, i = 0, n = 0, nulls = 0;

	// allocate memory
	for(j=0; j<bytes; j++)
		if(xpm[j] == 0x0)
			nulls++;
	xpm_pixmap = malloc((nulls + 1) * sizeof(char*));

	// copy memory
	while(i<bytes)
	{
		// find number of characters
		c = 0;
		while(xpm[c+i])
			c++;
		xpm_pixmap[n] = malloc(c+1);
		memcpy(xpm_pixmap[n], &xpm[i], c+1);

		i += c + 1;
		n++;
	}
	xpm_pixmap[n] = NULL;

	// add image
	client->image = realloc(client->image, sizeof(image_t) * (client->n_images+1));
	client->image[client->n_images].n = number;
	client->image[client->n_images].pixmap = load_xpm(xpm_pixmap);
	client->n_images++;

	free(xpm_pixmap);

	return -1;
}

int draw_image(client_t* client, int n, int x, int y)
{
	int i;
	Pixmap pixmap = 0;
	Window tmpw;
	int t;
	unsigned int w, h, ut;

	// get image
	int found = 0;
	for(i=0; i<client->n_images; i++)
		if(client->image[i].n == n)
		{
			found = 1;
			pixmap = client->image[i].pixmap;
		}
	if(!found)
		return 0;

	// draw image
	XGetGeometry(display, pixmap, &tmpw, &t, &t,
			&w, &h, &ut, &ut);
	XCopyArea(display, pixmap, client->pixmap, client->gc,
			0, 0, w, h, x, y);

	return -1;
}

int draw_panel(client_t* client, int theme, int x, int y, int x2, int y2)
{
	int w, h;
	get_size(&x, &y, &x2, &y2, &w, &h);

	XSetForeground(display, client->gc, client->color[XD_GRAY_90].pixel);
	XSetBackground(display, client->gc, client->color[XD_GRAY_90].pixel);
	XFillRectangle(display, client->pixmap, client->gc, x, y, w, h);

	XSetForeground(display, client->gc, client->color[XD_BLACK].pixel);
	XSetBackground(display, client->gc, client->color[XD_BLACK].pixel);
	XFillRectangle(display, client->pixmap, client->gc, x, y, w-1, h-1);

	XSetForeground(display, client->gc, themes[theme].bg.pixel);
	XSetBackground(display, client->gc, themes[theme].bg.pixel);
	XFillRectangle(display, client->pixmap, client->gc, x+1, y+1, w-2, h-2);

	return -1;
}

int draw_led_panel(client_t* client, int x, int y, int x2, int y2)
{
	int w, h;
	get_size(&x, &y, &x2, &y2, &w, &h);

	XSetForeground(display, client->gc, client->color[XD_GRAY_90].pixel);
	XSetBackground(display, client->gc, client->color[XD_GRAY_90].pixel);
	XFillRectangle(display, client->pixmap, client->gc, x, y, w, h);

	XSetForeground(display, client->gc, client->color[XD_BLACK].pixel);
	XSetBackground(display, client->gc, client->color[XD_BLACK].pixel);
	XFillRectangle(display, client->pixmap, client->gc, x, y, w-1, h-1);

	XSetForeground(display, client->gc, client->color[XD_LED_BG].pixel);
	XSetBackground(display, client->gc, client->color[XD_LED_BG].pixel);
	XFillRectangle(display, client->pixmap, client->gc, x+1, y+1, w-2, h-2);

	return -1;
}

void move_client(client_t* client)
{
	XMoveWindow(display, client->window, screen_w - 64, client->position * 64 + y_skip);
}

static void create_faux_window()
{
	Atom atoms[2] = { None, None };
	Colormap colormap = DefaultColormap(display, 0);
	XParseColor(display, colormap, "#e6e6e6", &faux_bg);
	XAllocColor(display, colormap, &faux_bg);

	faux_window = XCreateSimpleWindow(
			display,
			DefaultRootWindow(display),
			screen_w - 64, 100, // position
			64, 64,
			0,
			faux_bg.pixel, faux_bg.pixel);

	atoms[0] = XInternAtom(display, "_NET_WM_WINDOW_TYPE_DOCK", False);
	XChangeProperty(display, faux_window,
			XInternAtom(display, "_NET_WM_WINDOW_TYPE", False),
			XA_ATOM, 32, PropModeReplace,
			(unsigned char*) atoms,
			1);
}

void draw_faux_window(int position)
{
	if(position == -1)
		XUnmapWindow(display, faux_window);
	else
	{
		XMoveWindow(display, faux_window, screen_w - 64,
				(position * 64) + y_skip);
		XMapWindow(display, faux_window);
		XLowerWindow(display, faux_window); // send to back
	}
}
