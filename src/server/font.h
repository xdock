#ifndef FONT_H
#define FONT_H

#include <X11/Xlib.h>
#include "../lib/xdock.h"

//Pixmap font[N_FONTS][255];

typedef struct
{
	int theme, n_font;
	Pixmap font[255];
	void* next;
} font_t;

font_t* first_font;

void load_fonts();

Pixmap* prepare_font(int theme, int font);

#endif
