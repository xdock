#include <stdlib.h>

#include "font.h"
#include "widget.h"
#include "font_led1.xpm"
#include "theme.h"

void load_fonts()
{
	first_font = NULL;
}

static Pixmap load_letter(char** xpm, int theme)
{
	char* s;

	s = malloc(12);
	sprintf(s, " \tc #%02X%02X%02X", themes[theme].bg_r, themes[theme].bg_g, themes[theme].bg_b);
	xpm[1] = s;

	s = malloc(12);
	sprintf(s, ".\tc #%02X%02X%02X", themes[theme].shadow_r, themes[theme].shadow_g, themes[theme].shadow_b);
	xpm[2] = s;

	s = malloc(12);
	sprintf(s, "+\tc #%02X%02X%02X", themes[theme].penumbra_r, themes[theme].penumbra_g, themes[theme].penumbra_b);
	xpm[3] = s;

	s = malloc(12);
	sprintf(s, "o\tc #%02X%02X%02X", themes[theme].light_r, themes[theme].light_g, themes[theme].light_b);
	xpm[4] = s;



	return load_xpm(xpm);
}

static void add_font(int theme, int font)
{
	font_t* new_font;
	int i;

	if(first_font == NULL)
	{
		first_font = malloc(sizeof(font_t));
		new_font = first_font;
	}
	else
	{
		font_t* current = first_font;
		while(current->next)
			current = current->next;
		current->next = malloc(sizeof(font_t));
		new_font = current->next;
	}
	new_font->theme = theme;
	new_font->n_font = font;
	new_font->next = NULL;

	for(i=0; i<255; i++)
		new_font->font[i] = 0;
	
	if(font == XD_FONT_MEDIUM)
	{
		new_font->font[' '] = load_letter(font_led_space, theme);
		new_font->font['A'] = load_letter(font_led_A, theme);
		new_font->font['B'] = load_letter(font_led_B, theme);
		new_font->font['C'] = load_letter(font_led_C, theme);
		new_font->font['D'] = load_letter(font_led_D, theme);
		new_font->font['E'] = load_letter(font_led_E, theme);
		new_font->font['F'] = load_letter(font_led_F, theme);
		new_font->font['G'] = load_letter(font_led_G, theme);
		new_font->font['H'] = load_letter(font_led_H, theme);
		new_font->font['I'] = load_letter(font_led_I, theme);
		new_font->font['J'] = load_letter(font_led_J, theme);
		new_font->font['K'] = load_letter(font_led_K, theme);
		new_font->font['L'] = load_letter(font_led_L, theme);
		new_font->font['M'] = load_letter(font_led_M, theme);
		new_font->font['N'] = load_letter(font_led_N, theme);
		new_font->font['O'] = load_letter(font_led_O, theme);
		new_font->font['P'] = load_letter(font_led_P, theme);
		new_font->font['Q'] = load_letter(font_led_Q, theme);
		new_font->font['R'] = load_letter(font_led_R, theme);
		new_font->font['S'] = load_letter(font_led_S, theme);
		new_font->font['T'] = load_letter(font_led_T, theme);
		new_font->font['U'] = load_letter(font_led_U, theme);
		new_font->font['V'] = load_letter(font_led_V, theme);
		new_font->font['W'] = load_letter(font_led_W, theme);
		new_font->font['X'] = load_letter(font_led_X, theme);
		new_font->font['Y'] = load_letter(font_led_Y, theme);
		new_font->font['Z'] = load_letter(font_led_Z, theme);
		new_font->font['1'] = load_letter(font_led_one, theme);
		new_font->font['2'] = load_letter(font_led_2, theme);
		new_font->font['3'] = load_letter(font_led_3, theme);
		new_font->font['4'] = load_letter(font_led_4, theme);
		new_font->font['5'] = load_letter(font_led_5, theme);
		new_font->font['6'] = load_letter(font_led_6, theme);
		new_font->font['7'] = load_letter(font_led_7, theme);
		new_font->font['8'] = load_letter(font_led_8, theme);
		new_font->font['9'] = load_letter(font_led_9, theme);
		new_font->font['0'] = load_letter(font_led_0, theme);
		new_font->font['!'] = load_letter(font_led_exclamation, theme);
		new_font->font['@'] = load_letter(font_led_at, theme);
		new_font->font['$'] = load_letter(font_led_dollar, theme);
		new_font->font['%'] = load_letter(font_led_percent, theme);
		new_font->font['+'] = load_letter(font_led_plus, theme);
		new_font->font['-'] = load_letter(font_led_minus, theme);
		new_font->font['/'] = load_letter(font_led_slash, theme);
		new_font->font['\\'] = load_letter(font_led_backslash, theme);
		new_font->font[(unsigned char)'�'] = load_letter(font_led_degree, theme);
		new_font->font['?'] = load_letter(font_led_question, theme);
		new_font->font['<'] = load_letter(font_led_lt, theme);
		new_font->font['>'] = load_letter(font_led_gt, theme);
		new_font->font['='] = load_letter(font_led_eq, theme);
		new_font->font['.'] = load_letter(font_led_dot, theme);
		new_font->font[':'] = load_letter(font_led_colon, theme);
		new_font->font[','] = load_letter(font_led_comma, theme);
		new_font->font[';'] = load_letter(font_led_semicolon, theme);
	}
}

Pixmap* prepare_font(int theme, int font)
{
	font_t* current;
	int found;
init:
	found = 0;
	current = first_font;
	while(current)
	{
		if(current->theme == theme && current->n_font == font)
			return current->font;
		current = current->next;
	}
	if(!found)
	{
		add_font(theme, font);
		goto init;
	}

	return NULL; // should not get here
}
