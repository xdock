#include "client.h"

void add_client(client_t* client)
{
	if(!first_client)
		first_client = client;
	else
	{
		client_t* current = first_client;
		while(current->next)
			current = current->next;
		current->next = client;
	}
	client->next = NULL;
}

void remove_client(client_t* client)
{
	if(first_client == NULL)
		return;
	else if(first_client == client)
	{
		if(first_client->next == NULL)
			first_client = NULL;
		else
			first_client = first_client->next;
	}
	else
	{
		client_t* previous = first_client;
		while(previous->next != client)
			previous = previous->next;
		previous->next = client->next;
	}
}
