#include <stdio.h>

#include "theme.h"
#include "widget.h"
#include "../lib/xdock.h"

extern Display* display;

void initialize_themes()
{
	theme_colormap = DefaultColormap(display, 0);

	set_theme(XD_THEME_GREEN,
			0x1b, 0x1b, 0x1b,
			0x00, 0x48, 0x40,
			0x00, 0x7c, 0x70,
			0x20, 0xb0, 0xa8);
	set_theme(XD_THEME_LCD,
			0x8e, 0x96, 0x8a,
			0x76, 0x7c, 0x6f,
			0x3e, 0x46, 0x3e,
			0x2, 0x2, 0x2);
}

void set_theme(int n,
		int bg_r, int bg_g, int bg_b,
		int shadow_r, int shadow_g, int shadow_b,
		int penumbra_r, int penumbra_g, int penumbra_b,
		int light_r, int light_g, int light_b)
{
	char s[10];

	themes[n].bg_r = bg_r;
	themes[n].bg_g = bg_g;
	themes[n].bg_b = bg_b;
	themes[n].shadow_r = shadow_r;
	themes[n].shadow_g = shadow_g;
	themes[n].shadow_b = shadow_b;
	themes[n].penumbra_r = penumbra_r;
	themes[n].penumbra_g = penumbra_g;
	themes[n].penumbra_b = penumbra_b;
	themes[n].light_r = light_r;
	themes[n].light_g = light_g;
	themes[n].light_b = light_b;

	sprintf(s, "#%02X%02X%02X", bg_r, bg_g, bg_b);
	XParseColor(display, theme_colormap, s, &themes[n].bg);
	XAllocColor(display, theme_colormap, &themes[n].bg);

	sprintf(s, "#%02X%02X%02X", shadow_r, shadow_g, shadow_b);
	XParseColor(display, theme_colormap, s, &themes[n].shadow);
	XAllocColor(display, theme_colormap, &themes[n].shadow);

	sprintf(s, "#%02X%02X%02X", penumbra_r, penumbra_g, penumbra_b);
	XParseColor(display, theme_colormap, s, &themes[n].penumbra);
	XAllocColor(display, theme_colormap, &themes[n].penumbra);

	sprintf(s, "#%02X%02X%02X", light_r, light_g, light_b);
	XParseColor(display, theme_colormap, s, &themes[n].light);
	XAllocColor(display, theme_colormap, &themes[n].light);
}
