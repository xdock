#ifndef NETWORK_H
#define NETWORK_H

#include "client.h"

void *network_listen(void* ptr);
void send_data(client_t* client, int length, unsigned char* data);

#endif
