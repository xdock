#ifndef COMMAND_H
#define COMMAND_H

#include "client.h"

int run_command(client_t* client, unsigned char* command, int bytes);

#endif
