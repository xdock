#ifndef CLIENT_H
#define CLIENT_H

#include <X11/Xlib.h>
#include <pthread.h>

typedef struct {
	int n;
	Pixmap pixmap;
} image_t;

typedef struct {
	int sock;
	int position;
	unsigned char* input;
	int bytes;

	pthread_t thread;

	Window window;
	GC gc;
	XColor color[255];
	Colormap colormap;
	Pixmap pixmap;

	unsigned char event_mask;
	unsigned char ev_x1, ev_x2, ev_y1, ev_y2;
	int last_pressed_x, last_pressed_y;

	image_t* image;
	int n_images;
	void* next;
} client_t;

client_t* first_client;

void add_client(client_t* client);
void remove_client(client_t* client);

#endif
