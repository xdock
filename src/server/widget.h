#ifndef WIDGET_H
#define WIDGET_H

#include <X11/Xlib.h>

#include "client.h"

int max_positions;
int y_skip;

// X Window
Pixmap load_xpm(char** xpm);
int create_window();
int draw_new_client(client_t* client);
void undraw_client(client_t* client);
void do_events();
int draw_update(client_t* client);
void move_client(client_t* client);
void draw_faux_window(int position);

// No theme
int draw_pixel(client_t* client, int color, int x, int y);
int draw_line(client_t* client, int color, int x1, int y1, int x2, int y2);
int draw_rectangle(client_t* client, int color, int x1, int y1, int x2, int y2);
int draw_box(client_t* client, int fgcolor, int bgcolor, int x1, int y1, int x2, int y2);
int move_box(client_t* client, int x, int y, int w, int h, int direction, int step, int bg_color);
int set_color(client_t* client, int color, int r, int g, int b);
int write_text(client_t* client, int theme, int font, int x, int y, unsigned char *text);
int new_pixmap(client_t* client, int n, int bytes, unsigned char* xpm);
int draw_image(client_t* client, int n, int x, int y);

// theme
int draw_panel(client_t* client, int theme, int x, int y, int x2, int y2);

// deprecated
int draw_led_panel(client_t* client, int x, int y, int w, int h);

#endif
