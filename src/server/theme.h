#ifndef THEME_H
#define THEME_H

#include <X11/Xlib.h>

Colormap theme_colormap;

typedef struct
{
	int bg_r, bg_g, bg_b;
	int shadow_r, shadow_g, shadow_b;
	int penumbra_r, penumbra_g, penumbra_b;
	int light_r, light_g, light_b;
	XColor bg, shadow, penumbra, light;
} theme_t;

theme_t themes[32];

void initialize_themes();

void set_theme(int n,
	int bg_r, int bg_g, int bg_b,
	int shadow_r, int shadow_g, int shadow_b,
	int penumbra_r, int penumbra_g, int penumbra_b,
	int light_r, int light_g, int light_b);

#endif
