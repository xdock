#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <resolv.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#include "network.h"
#include "client.h"
#include "command.h"
#include "widget.h"

static int sock;

void parse_input(client_t* client, unsigned char* input, int bytes)
{
	int found;
	int i;

#if 0
	int k = 0;
	printf("Listened (%d): ", bytes);
	while(k < bytes)
	{
		if(input[k] < 32 || input[k] > 254)
			printf("%02X ", input[k]);
		else
			printf(" %c ", input[k]);
		k++;
	}
	printf("\n");
#endif

	// move data into 'input'
	client->input = realloc(client->input, client->bytes + bytes);
	memcpy(&client->input[client->bytes], input, bytes);
	client->bytes += bytes;

	do
	{
		// check for a EOF (0xFF)
		found = 0;
		for(i=0; i<client->bytes; i++)
			if(client->input[i] == 0xFF)
			{
				found = 1;
				break;
			}

		// if found, build the string
		if(found)
		{
			i = 0;
			unsigned char* parameter = malloc(1);
			while(client->input[i] != 0xFF) // 0xff was already found earlier
			{
				parameter = realloc(parameter, i+1);
				parameter[i] = client->input[i];
				i++;
			}

			// clear string
			unsigned char* tmp = malloc(client->bytes - i -1);
			memcpy(tmp, &client->input[i+1], client->bytes - i - 1);
			free(client->input);
			client->input = tmp;
			client->bytes -= (i + 1);

#if 0
	k = 0;
	printf("Command (%d): ", i);
	while(k < i)
	{
		if(parameter[k] < 32 || parameter[k] > 254)
			printf("%02X ", parameter[k]);
		else
			printf(" %c ", parameter[k]);
		k++;
	}
	printf("\n");
#endif

			// run command
			run_command(client, parameter, i);
		}

	} while(found);
}

void *new_client(void* ptr)
{
	unsigned char input[100];
	int bytes;
	client_t* current;
	int i;

	client_t* client = (client_t*)ptr;
	printf("Client connected on socket %d.\n", client->sock);

	client->input = malloc(1);
	client->bytes = 0;
	client->last_pressed_x = client->last_pressed_y = -1;
	client->event_mask = 0;
	
	// get position
	int found;
	client->position = -1;
	for(i=0; i<max_positions; i++)
	{
		client_t* current = first_client;

		found = 0;
		while(current)
		{
			if(current->position == i)
				found = 1;
			current = current->next;
		}
		if(!found)
		{
			client->position = i;
			break;
		}
	}
	printf("position: %d\n", client->position);

	client->n_images = 0;
	add_client(client);

	draw_new_client(client);

	// listen
	do
	{
		bytes = recv(client->sock, input, 100, 0);
		if(bytes > 0)
			parse_input(client, input, bytes);
	}
	while(bytes > 0);

	undraw_client(client);

	remove_client(client);
	free(client->input);
	free(client);
	// TODO free everything else

	current = first_client;
	i = 0;
	while(current)
	{
		current->position = i;
		move_client(current);
		i++;
		current = current->next;
	}

	return NULL;
}

void *network_listen(void* ptr)
{
	int connect_socket;
	struct sockaddr_in address, client_address;
	unsigned int client_address_length;
	pthread_t* thread;
	client_t* client;

	first_client = NULL;

	// create socket
	if((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		fprintf(stderr, "Error opening socket.\n");
		exit(1);
	}

	address.sin_family = AF_INET;
	address.sin_port = htons(52530);
//	address.sin_addr.s_addr = inet_addr("127.0.0.1");
	address.sin_addr.s_addr = htonl(INADDR_ANY); // TODO - listen localhost only

	// bind socket
	if(bind(sock, (struct sockaddr*)&address, sizeof(address)) < 0)
	{
		fprintf(stderr, "Can't bind socket.\n");
		exit(1);
	}

	// listen on socket
	listen(sock, 5);

	// accept incoming connections
	for(;;)
	{
		client_address_length = sizeof(client_address);
		connect_socket = accept(sock, (struct sockaddr*)&client_address, &client_address_length);
		thread = malloc(sizeof(pthread_t));
		client = malloc(sizeof(client_t));
		client->sock = connect_socket;
		pthread_create(thread, NULL, new_client, client);
	}
}

void send_data(client_t* client, int length, unsigned char* data)
{
	send(client->sock, data, length, 0);
}
