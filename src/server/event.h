#ifndef EVENT_H
#define EVENT_H

#include "client.h"

void mouse_down(client_t* client, int x, int y, int button);
void mouse_up(client_t* client, int x, int y, int button, int x_root, int y_root);
void mouse_move(client_t* client, int x, int y, unsigned char button_mask, int x_root, int y_root);
void grab_events(client_t* client, unsigned char mask, int x, int y, int x2, int y2);

#endif
