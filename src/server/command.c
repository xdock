#include "command.h"
#include "network.h"
#include "widget.h"
#include "event.h"

#include <stdio.h>

int check_parameters(unsigned char* command, int bytes, int correct_min, int correct_max)
{
	if(bytes-1 < correct_min || bytes-1 > correct_max)
	{
		int i;

		fprintf(stderr, "Incorrect protocol command:");
		for(i=0; i<bytes; i++)
			fprintf(stderr, " %02X", command[i]);
		fprintf(stderr, "\n");
		return 0;
	}
	else
		return -1;
}

int run_command(client_t* client, unsigned char* command, int bytes)
{
	switch(command[0])
	{
		case 0x0: // NOP
			if(check_parameters(command, bytes, 0, 0))
				draw_update(client);
			break;

		case 0x1: // draw pixel
			if(check_parameters(command, bytes, 3, 3))
				draw_pixel(client, command[1], command[2], command[3]);
			break;

		case 0x2: // draw line
			if(check_parameters(command, bytes, 5, 5))
				draw_line(client, command[1], command[2], command[3], command[4], command[5]);
			break;

		case 0x8: // draw rectangle
			if(check_parameters(command, bytes, 5, 5))
				draw_rectangle(client, command[1], command[2], command[3], command[4], command[5]);
			break;

		case 0x9: // draw box
			if(check_parameters(command, bytes, 6, 6))
				draw_box(client, command[1], command[2], command[3], command[4], command[5], command[6]);
			break;

		case 0x3: // move box
			if(check_parameters(command, bytes, 7, 7))
				move_box(client, command[1], command[2], command[3], command[4], command[5], command[6], command[7]);
			break;

		case 0x7: // set color
			if(check_parameters(command, bytes, 4, 4))
				set_color(client, command[1], command[2], command[3], command[4]);
			break;

		case 0x4: // write
			command[bytes] = 0; // I don't know why
			// printf("command -> %s\n", &command[4]);
			if(check_parameters(command, bytes, 6, 100))
				write_text(client, command[1], command[2], command[3], command[4], &command[5]);
			break;

		case 0x5: // new XPM
			if(check_parameters(command, bytes, 3, 100000))
				new_pixmap(client, command[1], bytes - 2, &command[2]);
			break;

		case 0x6: // draw image
			if(check_parameters(command, bytes, 3, 3))
				draw_image(client, command[1], command[2], command[3]);
			break;

		case 0x0A: // grab events
			if(check_parameters(command, bytes, 5, 5))
				grab_events(client, command[1], command[2], command[3], command[4], command[5]);
			break;

		case 0xA0: // draw 3D box (DEPRECATED)
			if(check_parameters(command, bytes, 4, 4))
				draw_led_panel(client, command[1], command[2], command[3], command[4]);
			break;

		case 0xA1: // draw 3D box
			if(check_parameters(command, bytes, 5, 5))
				draw_panel(client, command[1], command[2], command[3], command[4], command[5]);
			break;

		default:
			check_parameters(command, bytes, 2, 1);
			return 0;
	}

	return 1;
}
