#include <stdlib.h>
#include <X11/Xlib.h>

#include "event.h"
#include "network.h"
#include "widget.h"
#include "../lib/xdock.h"

extern Display* display;
static int dragging = 0;
static int faux_position;

void mouse_press(client_t* client, int x, int y, int button)
{
	if(x >= client->ev_x1 && x <= client->ev_x2
	&& y >= client->ev_y1 && y <= client->ev_y2)
	{
		if(client->event_mask & XD_MOUSE_PRESS)
		{
			unsigned char c[4] = { XD_MOUSE_PRESS, (unsigned char)x, (unsigned char)y, (unsigned char)button };
			send_data(client, 4, c);
		}
	}
}

void mouse_down(client_t* client, int x, int y, int button)
{
	if(button > 3) // no wheel
		return;

	if(x >= client->ev_x1 && x <= client->ev_x2
	&& y >= client->ev_y1 && y <= client->ev_y2)
	{
		client->last_pressed_x = x;
		client->last_pressed_y = y;
		if(client->event_mask & XD_MOUSE_DOWN)
		{
			unsigned char c[4] = { XD_MOUSE_DOWN, (unsigned char)x, (unsigned char)y, (unsigned char)button };
			send_data(client, 4, c);
		}
	}
	else if(button == 1)
		dragging = 1;
}

void mouse_up(client_t* client, int x, int y, int button, int x_root, int y_root)
{
	if(button > 3) // no wheel
		return;

	if(client->last_pressed_x != -1)
	{
		if(abs(x - client->last_pressed_x) <= 5 && abs(y - client->last_pressed_y) <= 5)
			mouse_press(client, client->last_pressed_x, client->last_pressed_y, button);
		client->last_pressed_x = client->last_pressed_y = -1;
	}

	if(button == 1 && dragging) // the user stopped dragging
	{
		dragging = 0;
		faux_position = -1;
		draw_faux_window(-1);

		// check if a position is already occupied
		{
			int is_occ = 0;
			client_t* current = first_client;
			while(current)
			{
				if(current->position == (y_root - y_skip) / 64)
					is_occ = 1;
				current = current->next;
			}
			if(!is_occ)
				client->position = (y_root - y_skip) / 64;
		}
		move_client(client);
	}
	else if(x >= client->ev_x1 && x <= client->ev_x2
	     && y >= client->ev_y1 && y <= client->ev_y2)
	{
		if(client->event_mask & XD_MOUSE_UP)
		{
			unsigned char c[4] = { XD_MOUSE_UP, (unsigned char)x, (unsigned char)y, (unsigned char)button };
			send_data(client, 4, c);
		}
	}
}

void mouse_move(client_t* client, int x, int y, unsigned char button_mask, int x_root, int y_root)
{
	if(button_mask == 0x1 && dragging)
	{
		// dragging = 1;
		XRaiseWindow(display, client->window);
		XMoveWindow(display, client->window, x_root - 32, y_root - 32);
		faux_position = (y_root - y_skip) / 64;
		draw_faux_window(faux_position);
	} 
	else if(x >= client->ev_x1 && x <= client->ev_x2
	&& y >= client->ev_y1 && y <= client->ev_y2)
	{
		// TODO send event to client
	}
}

void grab_events(client_t* client, unsigned char mask, int x, int y, int x2, int y2)
{
	client->event_mask = mask;
	client->ev_x1 = x;
	client->ev_x2 = x2;
	client->ev_y1 = y;
	client->ev_y2 = y2;
}
