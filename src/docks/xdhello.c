#include <xdock.h>

int main()
{
	xd_connect();
	xd_draw_panel(XD_THEME_LCD, 4, 4, 60, 60);
	xd_write(XD_THEME_LCD, XD_FONT_MEDIUM, 5, 5, "HELLO ");
	xd_write(XD_THEME_LCD, XD_FONT_MEDIUM, 5, 14, "WORLD!");
	xd_flush();
	for(;;);
}
