/* Based upon miniCHESS <http://insanum.com/miniCHESS/> */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <xdock.h>
#include "xdchess.xpm"

// colors
#define WHITE_SQ   0xA0
#define BLACK_SQ   0xA1

void draw_board();

unsigned char board[8][8];
char turn, me;
int selected_x = -1, selected_y = -1;
char COMMAND_FILE[255];
char SAVE_FILE[255];

void read_board()
{
	FILE* f;
	char s[200];
	int i, k, x, y;

	f = fopen(SAVE_FILE, "r");
	while(fgets(s, 200, f));
	
	x = y = i = 0;
	while(s[i] != ' ')
	{
		if(s[i] >= '1' && s[i] <= '8')
		{
			for(k=0; k<(s[i]-'0'); k++)
			{
				board[x][y] = ' ';
				x++;
			}
		}
		else if(s[i] == '/')
		{
			x = 0; 
			y++; 
		}
		else
		{
			board[x][y] = s[i];
			x++;
		}
		i++;
	}
	turn = s[i+1];
	fclose(f);

	// For some reason I don't understand, gnuchess appends to the save
	// file instead of overwriting it. So, we must do that ourselves.
	f = fopen(SAVE_FILE, "w");
	fprintf(f, "%s", s);
	fclose(f);
}

void do_command(char* command)
{
	FILE* f;
	struct stat buf;
	char cmd[300];

	f = fopen(COMMAND_FILE, "w");
	if(stat(SAVE_FILE, &buf) == 0)
		fprintf(f, "epdload %s\n", SAVE_FILE);
	if(command)
		fprintf(f, "%s\n", command);
	fprintf(f, "epdsave %s\n", SAVE_FILE);
	fprintf(f, "quit\n");
	fclose(f);

	sprintf(cmd, "gnuchess -x -m < %s > /dev/null", COMMAND_FILE);
	if(system(cmd))
	{
		fprintf(stderr, "gnuchess was not found in this system. This software is required to run xdchess. If it's indeed installed, check that it's in the path.\n");
		exit(1);
	}
}

void select_square(int x, int y)
{
	if((turn == 'w' && board[x][y] >= 'A' && board[x][y] <= 'Z')
	|| (turn == 'b' && board[x][y] >= 'a' && board[x][y] <= 'z'))
	{
		selected_x = x;
		selected_y = y;
		draw_board();
	}
}

void move_to(int x, int y)
{
	char move[5];

	// unselect
	if(selected_x == x && selected_y == y)
	{
		selected_x = selected_y = -1;
		draw_board();
		return;
	}

	// do move
	move[0] = selected_x + 'a';
	move[1] = (7 - selected_y) + '1';
	move[2] = x + 'a';
	move[3] = (7 - y) + '1';
	move[4] = 0;

	selected_x = selected_y = -1;
	do_command(move);
	draw_board();
	if(turn != me)
	{
		do_command("go");
		draw_board();
	}
}

void draw_board()
{
	int x, y;
	read_board();
	
	for(x=0; x<8; x++)
		for(y=0; y<8; y++)
		{
			unsigned char color;
			color = ((x+y) % 2 ? WHITE_SQ : BLACK_SQ);
			if(board[x][y] == ' ')
				xd_draw_box(color, color, (x*7)+4, (y*7)+4, (x*7)+10, (y*7)+10);
			else
			{
				if(selected_x == x && selected_y == y)
					xd_draw_image(board[x][y]+80, (x*7)+4, (y*7)+4);
				else
					xd_draw_image(board[x][y], (x*7)+4, (y*7)+4);
			}
		}
	xd_flush();
}

void send_pieces()
{
	// white pieces
	xd_send_xpm('K', king_xpm);
	xd_send_xpm('Q', queen_xpm);
	xd_send_xpm('R', rook_xpm);
	xd_send_xpm('N', knight_xpm);
	xd_send_xpm('B', bishop_xpm);
	xd_send_xpm('P', pawn_xpm);

	// black pieces
	king_xpm[1]   = "  c #000000"; king_xpm[2]   = "+ c #FFFFFF";
	queen_xpm[1]  = "  c #000000"; queen_xpm[2]  = "+ c #FFFFFF";
	rook_xpm[1]   = "  c #000000"; rook_xpm[2]   = "+ c #FFFFFF";
	knight_xpm[1] = "  c #000000"; knight_xpm[2] = "+ c #FFFFFF";
	bishop_xpm[1] = "  c #000000"; bishop_xpm[2] = "+ c #FFFFFF";
	pawn_xpm[1]   = "  c #000000"; pawn_xpm[2]   = "+ c #FFFFFF";
	xd_send_xpm('k', king_xpm);
	xd_send_xpm('q', queen_xpm);
	xd_send_xpm('r', rook_xpm);
	xd_send_xpm('n', knight_xpm);
	xd_send_xpm('b', bishop_xpm);
	xd_send_xpm('p', pawn_xpm);

	// selected pieces
	king_xpm[1]   = "  c #0000FF";
	queen_xpm[1]  = "  c #0000FF";
	rook_xpm[1]   = "  c #0000FF";
	knight_xpm[1] = "  c #0000FF";
	bishop_xpm[1] = "  c #0000FF";
	pawn_xpm[1]   = "  c #0000FF";
	xd_send_xpm('k'+80, king_xpm);
	xd_send_xpm('q'+80, queen_xpm);
	xd_send_xpm('r'+80, rook_xpm);
	xd_send_xpm('n'+80, knight_xpm);
	xd_send_xpm('b'+80, bishop_xpm);
	xd_send_xpm('p'+80, pawn_xpm);
	xd_send_xpm('K'+80, king_xpm);
	xd_send_xpm('Q'+80, queen_xpm);
	xd_send_xpm('R'+80, rook_xpm);
	xd_send_xpm('N'+80, knight_xpm);
	xd_send_xpm('B'+80, bishop_xpm);
	xd_send_xpm('P'+80, pawn_xpm);

}

void initialize_dock()
{
	xd_connect();

	// colors
	xd_set_color(WHITE_SQ, 0xc8, 0xc3, 0x65);
	xd_set_color(BLACK_SQ, 0x77, 0xa2, 0x6d);

	// board
	xd_led_draw_panel(3, 3, 60, 60);
	send_pieces();
}

int main()
{
	char dir[255];
	xd_event_t event;

	// initialize dock
	initialize_dock();

	// initialize envoirnment
	sprintf(dir, "/tmp");
	sprintf(COMMAND_FILE, "%s/xdchess.%s", dir, getenv("USER"));
	sprintf(SAVE_FILE, "%s/xdchess.%s.epd", dir, getenv("USER"));
	mkdir(dir, S_IRUSR | S_IWUSR);
	// remove(SAVE_FILE);

	// initialize gnuchess
	me = 'w';
	do_command(NULL);
	draw_board();

	// get events
	xd_grab_events(XD_MOUSE_PRESS, 4, 4, 60, 60);
	for(;;)
	{
		if(xd_next_event(&event))
		{
			if(event.button == 1)
			{
				if(selected_x == -1)
					select_square((event.x-4)/7, (event.y-4)/7);
				else
					move_to((event.x-4)/7, (event.y-4)/7);
			}
			else if(event.button == 3)
			{
				do_command("new");
				draw_board();
			}
		}
	}
}
