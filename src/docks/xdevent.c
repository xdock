#include <xdock.h>

#define GREEN 0x80
#define RED   0x81

int main()
{
	xd_event_t event;

	xd_connect();
	xd_draw_box(XD_GRAY_50, XD_GRAY_50, 15, 15, 48, 48);
	xd_set_color(GREEN, 0, 255, 0);
	xd_set_color(RED, 255, 0, 0);
	xd_flush();

	xd_grab_events(XD_MOUSE_PRESS, 15, 15, 48, 48);
	while(1)
	{
		if(xd_next_event(&event))
		{
			if(event.button == 1)
				xd_draw_box(GREEN, GREEN, 15, 15, 48, 48);
			else
				xd_draw_box(RED, RED, 15, 15, 48, 48);
			xd_flush();
		}
	}
}
