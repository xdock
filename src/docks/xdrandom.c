#include <xdock.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
	int r;
	xd_connect();
	xd_led_draw_panel(5, 5, 58, 58);
	while(1)
	{
		r = 1 + (int)(55.0 * (rand() / (RAND_MAX + 1.0)));
		xd_move_box(6, 6, 57, 57, XD_LEFT, 1, XD_LED_BG);
		xd_draw_line(XD_LED_LIT_1, 57, 57, 57, r+4);
		xd_draw_point(XD_LED_GLOW, 57, 30);
		xd_flush();
		usleep(700000);
	}
}
