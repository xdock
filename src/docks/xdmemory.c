#include <xdock.h>
#include <stdio.h>
#include <stdlib.h>

void get_memory(int *free, int *used)
{
	FILE* f = fopen("/proc/meminfo", "r");
	char type[80], kb[80];
	int mem, total;

	if(!f)
	{
		fprintf(stderr, "Invalid operation: file /proc/meminfo is not accessible.\n");
		exit(1);
	}
	while(!feof(f))
	{
		fscanf(f, "%s %d %s", type, &mem, kb);
		if(strncmp(type, "MemTotal", 8) == 0)
			total = mem;
		else if(strncmp(type, "MemFree", 7) == 0)
			*free = mem;
	}
	fclose(f);
	*used = total - *free;
}

int main()
{
	int free, used;
	float y;
	char line1[50], line2[50];

	xd_connect();
	xd_led_draw_panel(4, 4, 60, 24);
	xd_led_draw_panel(4, 27, 60, 60);
	while(1)
	{
		get_memory(&free, &used);

		sprintf(line1, "FREE %4d", (free/1024));
		sprintf(line2, "USED %4d", (used/1024));
		xd_write(XD_THEME_GREEN, XD_FONT_MEDIUM, 5, 6, line1);
		xd_write(XD_THEME_GREEN, XD_FONT_MEDIUM, 5, 15, line2);

		xd_move_box(5, 28, 59, 59, XD_LEFT, 1, XD_LED_BG);

		y = 59 - (30.0 * ((float)(used) / (float)(free + used)));
		xd_draw_line(XD_LED_LIT_1, 58, (unsigned char)y, 58, 59);

		xd_flush();
		sleep(1);
	}
	return 0;
}
