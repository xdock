#ifndef APPLET_H
#define APPLET_H

#include <stddef.h>

typedef unsigned char uchar;

// Colors
#define XD_BLACK 0x00
#define XD_GRAY_10  0x01
#define XD_GRAY_20  0x02
#define XD_GRAY_30  0x03
#define XD_GRAY_40  0x04
#define XD_GRAY_50  0x05
#define XD_GRAY_60  0x06
#define XD_GRAY_70  0x07
#define XD_GRAY_80  0x08
#define XD_GRAY_90  0x09
#define XD_WHITE    0x0A

#define XD_LED_BG      0x0B
#define XD_LED_UNLIT_1 0x0C
#define XD_LED_UNLIT_2 0x0D
#define XD_LED_LIT_1   0x0E
#define XD_LED_LIT_2   0x0F
#define XD_LED_GLOW    0x10

// Themes
#define XD_THEME_GREEN 24
#define XD_THEME_LCD   25

// Directions
#define XD_UP    0x0
#define XD_DOWN  0x1
#define XD_LEFT  0x2
#define XD_RIGHT 0x3

// Fonts
#define XD_FONT_LED_1 0x0 // DEPRECATED
#define N_FONTS       0x1 // DEPRECATED
#define XD_FONT_MEDIUM 0x0

// Events
#define XD_MOUSE_DOWN   0x1
#define XD_MOUSE_UP     0x2
#define XD_MOUSE_PRESS  0x4
typedef struct
{
	unsigned char type;
	unsigned char x;
	unsigned char y;
	unsigned char button;
} xd_event_t;

// Connection
int xd_connect();
void xd_disconnect();
void xd_flush();
const char* xd_version();

// Basic operations
void xd_draw_point(uchar color, uchar x, uchar y);
void xd_draw_line(uchar color, uchar x1, uchar y1, uchar x2, uchar y2);
void xd_draw_rectangle(uchar color, uchar x1, uchar y1, uchar x2, uchar y2);
void xd_draw_box(uchar fgcolor, uchar bgcolor, uchar x1, uchar y1, uchar x2, uchar y2);
void xd_set_color(uchar color, uchar r, uchar g, uchar b);

// Advanced operations
void xd_move_box(uchar x1, uchar y1, uchar x2, uchar y2, uchar direction, uchar step, uchar bg_color);
void xd_send_xpm(uchar n, char** xpm);
void xd_draw_image(uchar n, uchar x, uchar y);

// themed operations
void xd_draw_panel(uchar theme, uchar x1, uchar y1, uchar x2, uchar y2);

// deprecated
void xd_led_draw_panel(uchar x1, uchar y1, uchar x2, uchar y2);
void xd_write(uchar theme, uchar font, uchar x, uchar y, const char* text);

// Events
void xd_grab_events(uchar event_mask, uchar x1, uchar y1, uchar x2, uchar y2);
int xd_next_event(xd_event_t* event);

#endif
