#include "network.h"

#ifdef WIN32
#	include <winsock.h>
#else
#	include <sys/socket.h>
#	include <sys/un.h>
#	include <netinet/in.h>
#	include <netdb.h>
#	include <fcntl.h>
#	define INVALID_SOCKET -1
#	define SOCKET_ERROR -1
#endif
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

int sock;

int open_connection(char* name)
{
	struct hostent* hostInfo;
	long hostAddress;
	struct sockaddr_in address;

#ifdef WIN32
	WORD wVersionRequested;
	WSADATA wsaData;
	wVersionRequested = MAKEWORD(1, 1);
	if(WSAStartup(wVersionRequested, &wsaData) != 0)
	{
		fprintf(stderr, "Could not open winsock.\n");
		return 0;
	}
#endif

	sock = socket(PF_INET, SOCK_STREAM, 0);
	if(sock == SOCKET_ERROR)
	{
		fprintf(stderr, "Could not make a socket.\n");
		return 0;
	}

	hostInfo = gethostbyname(name);
	if(!hostInfo)
	{
		fprintf(stderr, "Couldn't find host %s.\n", name);
		return 0;
	}

	memcpy(&hostAddress, hostInfo->h_addr, hostInfo->h_length);

	address.sin_addr.s_addr = hostAddress;
	address.sin_port = htons(52530);
	address.sin_family = AF_INET;
	memset(address.sin_zero, '\0', sizeof(address.sin_zero));

	if(connect(sock, (struct sockaddr*)&address, sizeof(address)) == SOCKET_ERROR)
	{
		fprintf(stderr, "Could not connect to applet server. Check if the applet server is running.\n");
		return 0;
	}
	else
	{
		fcntl(sock, F_SETFL, O_NONBLOCK); // set the socket as non-blocking
		return -1;
	}
}

int send_command(int bytes, ...)
{
	va_list(ap);
	int i;

	va_start(ap, bytes);
	unsigned char* data = malloc(bytes + 1);
	for(i=0; i<bytes; i++)
		data[i] = (unsigned char)va_arg(ap, int);
	data[bytes] = 0xff;
	va_end(ap);
	
	int sent = send(sock, data, bytes+1, 0);
	if(sent == -1)
		return 0;
	else
		return -1;
}

int send_without_eof(int bytes, ...)
{
	va_list(ap);
	int i;

	va_start(ap, bytes);
	unsigned char* data = malloc(bytes + 1);
	for(i=0; i<bytes; i++)
		data[i] = (unsigned char)va_arg(ap, int);
	va_end(ap);
	
	int sent = send(sock, data, bytes, 0);
	if(sent == -1)
		return 0;
	else
		return -1;
}

int send_bytes(size_t bytes, unsigned char* c)
{
	unsigned char eof[1] = { 0xff };

	send(sock, c, bytes, 0);
	int sent = send(sock, eof, 1, 0);
	if(sent == -1)
		return 0;
	else
		return -1;
}

unsigned char* read_data(int length)
{
	unsigned char* c = malloc(length);
	int i = recv(sock, c, length, 0);
	if(i == -1)
	{
		free(c);;
		return NULL;
	}
	else if(i == 0)
	{
		fprintf(stderr, "Connection lost.\n");
		exit(1);
	}
	else
		return c;
}

void close_connection()
{
#ifdef WIN32
	closesocket(sock);
#else
	if(close(sock) == SOCKET_ERROR)
		fprintf(stderr, "Could not close socket\n");
#endif
}
