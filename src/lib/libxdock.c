#include "xdock.h"
#include "network.h"
#include "../../config.h"

#include <string.h>
#include <stdio.h>

int cp(int var, int min, int max, char* var_name, char* function_name)
{
	if(var < min || var > max)
	{
		fprintf(stderr, "Error: Parameter %s (%d~%d) from function %s has value %d.\n", var_name, min, max, function_name, var);
		return 0;
	}
	else
		return -1;
}

int xd_connect()
{
	return open_connection("localhost");
}

void xd_disconnect()
{
	close_connection();
}

void xd_flush()
{
	send_command(1, 0x0);
}

const char* xd_version()
{
	return VERSION;
}

void xd_draw_point(uchar color, uchar x, uchar y)
{
	if(cp(x, 0, 63, "x", "xd_draw_point")
	&& cp(y, 0, 63, "y", "xd_draw_point"))
		send_command(4, 0x1, color, x, y);
}

void xd_draw_line(uchar color, uchar x1, uchar y1, uchar x2, uchar y2)
{
	if(cp(x1, 0, 63, "x1", "xd_draw_line")
	&& cp(y1, 0, 63, "y1", "xd_draw_line")
	&& cp(x2, 0, 63, "x2", "xd_draw_line")
	&& cp(y2, 0, 63, "y2", "xd_draw_line"))
		send_command(6, 0x2, color, x1, y1, x2, y2);
}

void xd_draw_rectangle(uchar color, uchar x1, uchar y1, uchar x2, uchar y2)
{
	if(cp(x1, 0, 63, "x1", "xd_draw_rectangle")
	&& cp(y1, 0, 63, "y1", "xd_draw_rectangle")
	&& cp(x2, 0, 63, "x2", "xd_draw_rectangle")
	&& cp(y2, 0, 63, "y2", "xd_draw_rectangle"))
		send_command(6, 0x8, color, x1, y1, x2, y2);
}

void xd_draw_box(uchar fgcolor, uchar bgcolor, uchar x1, uchar y1, uchar x2, uchar y2)
{
	if(cp(x1, 0, 63, "x1", "xd_draw_box")
	&& cp(y1, 0, 63, "y1", "xd_draw_box")
	&& cp(x2, 0, 63, "x2", "xd_draw_box")
	&& cp(y2, 0, 63, "y2", "xd_draw_box"))
		send_command(7, 0x9, fgcolor, bgcolor, x1, y1, x2, y2);
}

void xd_move_box(uchar x1, uchar y1, uchar x2, uchar y2, uchar direction, uchar step, uchar bg_color)
{
	if(cp(x1, 0, 63, "x1", "xd_move_box")
	&& cp(y1, 0, 63, "y1", "xd_move_box")
	&& cp(x2, 0, 63, "x2", "xd_move_box")
	&& cp(y2, 0, 63, "y2", "xd_move_box")
	&& cp(direction, 0, 3, "direction", "xd_move_box"))
		send_command(8, 0x3, x1, y1, x2, y2, direction, step, bg_color);
}

void xd_set_color(uchar color, uchar r, uchar g, uchar b)
{
	// can't send 0xFF since it's a EOF sign
	if(r >= 0x80)
		r--;
	if(g >= 0x80)
		g--;
	if(b >= 0x80)
		b--;

	if(cp(color, 0x80, 0xFF, "color", "xd_set_color"))
		send_command(5, 0x7, color, r, g, b);
}

void xd_write(uchar theme, uchar font, uchar x, uchar y, const char* text)
{
	if(cp(x, 0, 63, "x", "xd_write")
	&& cp(y, 0, 63, "y", "xd_write")
	&& cp(font, 0, N_FONTS-1, "font", "xd_write"))
	{
		send_without_eof(5, 0x4, theme, font, x, y);
		send_bytes(strlen(text), (unsigned char*)text);
	}
}

void xd_send_xpm(uchar n, char** xpm)
{
	int junk, h, colors;
	int i = 0;
	size_t ct = 0;
	char* str = malloc(1);

	// get xpm size
	sscanf(xpm[0], "%d %d %d", &junk, &h, &colors);

	for(i=0; i<(1+colors+h); i++)
	{
		ct += strlen(xpm[i])+1;
		str = realloc(str, ct);
		memcpy(&str[ct - (strlen(xpm[i])+1)], xpm[i], strlen(xpm[i])+1);
	}

	send_without_eof(2, 0x5, n);
	send_bytes(ct, str);
	free(str);
}

void xd_draw_image(uchar n, uchar x, uchar y)
{
	if(cp(x, 0, 63, "x", "xd_draw_image")
	&& cp(y, 0, 63, "y", "xd_draw_image"))
		send_command(4, 0x6, n, x, y);
}

void xd_led_draw_panel(uchar x1, uchar y1, uchar x2, uchar y2)
{
	if(cp(x1, 0, 63, "x1", "xd_led_draw_panel")
	&& cp(y1, 0, 63, "y1", "xd_led_draw_panel")
	&& cp(x2, 0, 63, "x2", "xd_led_draw_panel")
	&& cp(y2, 0, 63, "y2", "xd_led_draw_panel"))
		send_command(5, 0xA0, x1, y1, x2, y2);
}

void xd_draw_panel(uchar theme, uchar x1, uchar y1, uchar x2, uchar y2)
{
	if(cp(theme, 0, 31, "theme", "xd_draw_panel")
	&& cp(x1, 0, 63, "x1", "xd_draw_panel")
	&& cp(y1, 0, 63, "y1", "xd_draw_panel")
	&& cp(x2, 0, 63, "x2", "xd_draw_panel")
	&& cp(y2, 0, 63, "y2", "xd_draw_panel"))
		send_command(6, 0xA1, theme, x1, y1, x2, y2);
}

void xd_grab_events(uchar event_mask, uchar x1, uchar y1, uchar x2, uchar y2)
{
	if(cp(x1, 0, 63, "x1", "xd_grab_events")
	&& cp(y1, 0, 63, "y1", "xd_grab_events")
	&& cp(x2, 0, 63, "x2", "xd_grab_events")
	&& cp(y2, 0, 63, "y2", "xd_grab_events")
	&& cp(event_mask, 0, 7, "event_mask", "xd_grab_events"))
		send_command(6, 0x0A, event_mask, x1, y1, x2, y2);
}

int xd_next_event(xd_event_t* event)
{
	unsigned char* c = read_data(4);
	if(c == NULL)
		return 0;
	else
	{
		event->type = c[0];
		event->x = c[1];
		event->y = c[2];
		event->button = c[3];
		free(c);
		return 1;
	}
}
