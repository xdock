#ifndef NETWORK_H
#define NETWORK_H

#include <stdarg.h>
#include <stddef.h>

int open_connection(char* host);
int send_command(int bytes, ...);
int send_without_eof(int bytes, ...);
int send_bytes(size_t bytes, unsigned char* c);
unsigned char* read_data(int length);
void close_connection();

#endif
